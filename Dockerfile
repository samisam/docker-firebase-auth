FROM openjdk:11-jre-slim

WORKDIR /auth

RUN apt-get update && \
  apt-get install -y curl && \
  apt-get clean;
RUN curl -sL https://firebase.tools/bin/linux/latest -o /usr/local/bin/firebase
RUN chmod +x /usr/local/bin/firebase


EXPOSE 9099

COPY firebase.json ./

CMD [ "firebase", "emulators:start", "--only", "auth", "--project", "test"]
