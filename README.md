# Docker + Auth

A docker image that wraps the [Firebase Emulator](https://firebase.google.com/docs/rules/emulator-setup)


## Run

```
docker build . -t firebaseauth:latest
docker run -it -p 9099:9099 firebaseauth:latest
```

## Use it with docker-compose

```

version: '3.7'

services:
  app:
    build: .
    depends_on:
      - firestore
    volumes:
      - .:/app
      - node_modules:/app/node_modules
    environment:
      FIREBASE_AUTH_EMULATOR_HOST: auth:8080

  auth:
    image: registry.gitlab.com/samisam/docker-firebase-auth

volumes:
  node_modules:
```
